#!/bin/bash 

if [ "$1" == "-l" ]; then
    shift
cat <<EOF
    ipres=\$(ip route 2>&1) || ipres=\$(/usr/sbin/ip route 2>&1)
    gw=\$(echo "\$ipres" | grep default | cut -d " " -f 3 )
    echo "trying gateway \$gw ..."
    res=\$(curl -s -L "http://\$gw:1000/logout?")
    if [ -n "\$(echo \$res | grep 'Continue')" ]; then
        echo "Logged out"
        exit 0
    else
        echo "\$res"
        echo "Couldnt log out on gateway \$gw"
        exit 0
    fi
EOF
exit
fi

usewget=0
if [ "$1" == "-w" ]; then
    usewget=1
fi

source "$HOME/bin/randerv2/list"

RANDOM=$$$(date +%s)
# echo "$RANDOM"

cat <<EOF

    sel=${expressions[$RANDOM % ${#expressions[@]}]}
    echo "Trying \$sel"
    
EOF

if [ $usewget -eq 1 ]; then
cat <<EOF
    response=\$(wget -qO- --tries=1 -T 1 "http://8.8.8.8")
    if [ "\$?" -eq 4 ];then
        echo "Maybe already logged in?"
        exit 0
    fi
EOF
else
cat <<EOF
    response=\$(curl -s -L --connect-timeout 1 8.8.8.8)
    if [ "\$?" -eq 28 ];then
        echo "Maybe already logged in?"
        exit 0
    fi
EOF
fi
cat <<EOF
    magic=\$(echo "\$response" | grep magic | cut -d '"' -f6)
    if [ -z "\$magic" ]; then
        echo "Couldnt fetch magic from redirection"
        exit 0
    fi

    u=\${sel:0:9}
    p=\${sel:10:10}
EOF

if [ $usewget -eq 1 ]; then
cat <<EOF
    curlres=\$(wget --post-data="magic=\$magic&username=\$u&password=\$p" -qO- "http://8.8.8.8")
EOF
else
cat <<EOF
    curlres=\$(curl -s -L -d "magic=\$magic" -d "username=\$u" -d "password=\$p" "http://8.8.8.8")
EOF
fi

cat <<EOF
    if [ -n "\$(echo \$curlres | grep 'over limit')" ]; then
        echo "Overlimit"
    elif [ -n "\$(echo \$curlres | grep 'try again')" ]; then
        echo "Login failed: \"\$u:\$p\""
    elif [ -n "\$(echo \$curlres | grep 'To change your password')" ]; then
        echo "Logged in"
        exit 0
    else
        echo "Invalid result"
        echo "\$curlres"
        exit 1
    fi

EOF
