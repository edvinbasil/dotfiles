#!/bin/bash
time="45m"
if [ -n "$1" ];then
    if [ "$1" = "-d" ];then
        systemctl --user stop timedsuspend.timer
        exit 0
    fi
    time="$1"
fi
notify-send --urgency low "Timed Suspend" "Suspending system in $time"
systemd-run --user --unit timedsuspend --on-active="$time" systemctl suspend
