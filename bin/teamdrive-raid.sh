#!/bin/bash

logdir="$HOME/LOGS/teamdrive-raid"

_announce(){
   if  [ -t 1 ]; then 
       echo "$@"
   fi
}

_rclonesync(){
    src="$1"
    dest="$2"
    src_dst="${src#drive-sh-}--${dst#drive-sh-}"
    logfile="${src_dst//:/-}"

    all_args=("$@")
    rest_args=("${all_args[@]:2}")

    rclone sync \
        "$src" \
        "$dest" \
	--drive-server-side-across-configs \
        --log-level="INFO" \
        --log-file="$logdir/$logfile.log" \
        "${rest_args[@]}"

    _announce "sync complete $src-$dest"
}

backup(){
    # Usage backup <repo>
    # eg: backup earth
    repo="$1"
    [ -z "$repo" ] && echo "Repo $1 not provided" && return 1
    shift
    rest_args=("$@")

    dr1="drive-sh-edoff4:$repo"
    dr2="drive-sh-edoff6:$repo"
    dr3="drive-sh-edoff7:$repo"
    dr4="drive-sh-edoff8:$repo"

    _announce "sync $dr1-$dr2"
    _rclonesync "$dr1" "$dr2" "${rest_args[@]}"

    _announce "sync $dr2-$dr3"
    _rclonesync "$dr2" "$dr3" "${rest_args[@]}"

    _announce "sync $dr3-$dr4"
    _rclonesync "$dr3" "$dr4" "${rest_args[@]}"

}

# Syncing earth
#backup earth $@ &
#backup mercury $@ &

wait
_announce "Complete"
