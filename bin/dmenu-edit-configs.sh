#!/bin/bash

# Dmenu script for editing some of my more frequently edited config files.
# Stolen from DistroTube on YT and modified for my use :)


declare options=("alacritty
aliases
awesome
bspwm
carnagesvc
configedit
dunst
hosts
i3
kitty
lf
neovim
nginx-fileservers
nginx-revproxies
nginx-tor
picom
pistol
polybar
resolv
ssh
sxhkd
sxhkd_common
tmux
vim
xmonad
xresources
zsh
quit")

getchoice() {
    case "$choice" in
        quit)
            echo "Program terminated." && exit 1
        ;;
        alacritty)
            choice="$HOME/.config/alacritty/alacritty.yml"
        ;;
        alacritty)
            choice="$HOME/.aliases"
        ;;
        awesome)
            choice="$HOME/.config/awesome/rc.lua"
        ;;
        bspwm)
            choice="$HOME/.config/bspwm/bspwmrc"
        ;;
        carnagesvc)
            choice="$PD/Test/carnage_do_home/svc/assets/config.yml"
        ;;
        configedit)
            choice="$0"
        ;;
        dunst)
            choice="$HOME/.config/dunst/dunstrc"
        ;;
        hosts)
            choice="/etc/hosts"
            sudoedit=1
        ;;
        i3)
            choice="$HOME/.config/i3/config"
        ;;
        kitty)
            choice="$HOME/.config/kitty/kitty.conf"
        ;;
        lf)
            choice="$HOME/.config/lf/lfrc"
        ;;
        neovim)
            choice="$HOME/.config/nvim/init.vim"
        ;;
        nginx-fileservers)
            choice="/etc/nginx/sites-enabled/fileservers.conf"
            sudoedit=1
        ;;
        nginx-revproxies)
            choice="/etc/nginx/sites-enabled/revproxies.conf"
            sudoedit=1
        ;;
        nginx-tor)
            choice="/etc/nginx/sites-enabled/torboi.conf"
            sudoedit=1
        ;;
        picom)
            choice="$HOME/.config/picom/picom.conf"
        ;;
        pistol)
            choice="$HOME/.config/pistol/pistol.conf"
        ;;
        polybar)
            choice="$HOME/.config/polybar/config"
        ;;
        resolv)
            choice="/etc/resolv.conf"
            sudoedit=1
        ;;
        ssh)
            choice="$HOME/.ssh/config"
        ;;
        sxhkd)
            choice="$HOME/.config/sxhkd/sxhkdrc"
        ;;
        sxhkd_common)
            choice="$HOME/.config/sxhkd/common.sxhkdrc"
        ;;
        tmux)
            choice="$HOME/.tmux.conf"
        ;;
        vim)
            choice="$HOME/.vimrc"
        ;;
        xmonad)
            choice="$HOME/.xmonad/xmonad.hs"
        ;;
        xresources)
            choice="$HOME/.Xresources"
        ;;
        zsh)
            choice="$HOME/.zshrc"
        ;;
        *)
            exit 1
        ;;
    esac
    [ -n "$sudoedit" ] && editwith=sudoedit || editwith=$EDITOR
}

if [ -n "$1" ];then
    choice=$(echo -e "${options[@]}" | fzf -i --prompt 'Edit config file: ')
    getchoice
    "$editwith" "$choice"
else
    choice=$(echo -e "${options[@]}" | dmenu -i -p 'Edit config file: ')
    getchoice
    $TERMINAL -e "$editwith" "$choice"
fi
