#!/bin/bash

if [ "$(pactl info | grep analog)" ]; then
pactl set-card-profile 0 output:hdmi-stereo
else
pactl set-card-profile 0 output:analog-stereo+input:analog-stereo
fi
## Use pavucontrol for gui
