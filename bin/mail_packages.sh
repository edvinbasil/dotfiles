#!/bin/bash

packages=$(pacman -Qei | awk '/^Name/ { name=$3 } /^Groups/ { if ( $3 != "base" && $3 != "base-devel" ) { print name } }')
count=$(echo "$packages" | wc -l)
old_list_path="$HOME/outputs/LOGS/pacman_list_installed"

packagediff=$(echo "$packages" | diff "$old_list_path" - -u0 | grep -v '^@@')

# no difference
[ -z "$packagediff" ] && notify-send "Package List" "No updates" && exit 0

if sg_api_key=$(pass show sg/sendgrid_rs_and_msmtp);then
    export SENDGRID_RS_API_KEY="$sg_api_key"
else
    >&2 echo "Couldnt get api token"
    exit 1
fi

message=$(cat <<EOF 
<div align="center">
    <h1>Package Diff</h1>
    <br><hr><br>
    <pre>
        $packagediff
    </pre>
    <br><hr><br>
    <h2 align='center'>Total Packages: $count</h2>
EOF
)


# echo "$message"
sendgrid-rs \
    -f "Carnage Laptop <carnage@edvinbasil.com>"\
    -t "Edvin basil <edvinbasil+carnage@gmail.com>" \
    -s "Check your packages on Carnage" \
    --html "$message"

[ "$?" -eq 0 ] && (echo "$packages" > "$old_list_path")
