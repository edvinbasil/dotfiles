#!/bin/sh

set -e

if [ "$1" = "-t" ];then
    time_norm="`date -I` $2"
    timespec="--on-calendar=$time_norm"
    shift

    systemd-analyze calendar "$time_norm" | tail -n1
else
    timespec="--on-active=$1"
fi

title="$2"
[ -z "$title" ] && title="Alarm"

systemd-run --user\
    --timer-property="AccuracySec=1ms" \
    "$timespec" \
    notify-send \
        --urgency critical \
        --icon arts \
        "$title" \
