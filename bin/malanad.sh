#!/bin/bash
channellist=~/outputs/wireshark/channellist

srow=1
while true; do
    srow=$(
        cut -d'/' -f3 $channellist |
        rofi -dmenu -p "search" \
        -format 'd' -selected-row "$srow" \
        -font "Noto Sans 8" -padding 20 \
        -sidebar-mode
    )
    [ "$?" -ne 0 ] && exit

    channelurl=$(sed "${srow}q;d" $channellist)

    [ -z "$channelurl" ] && echo "no channel selected" && exit 0
    #mpv -fs "http://172.16.16.10$channelurl"
    mpv -fs "https://do.edvinbasil.com/tv/$channelurl"
done
