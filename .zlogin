[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

# Starts X automatically when loggen into tty1
if [ "$(tty)" = "/dev/tty1" ]; then
    pgrep -x i3 || exec startx

fi
