#. "$HOME/.zshrc"

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"

export MAILTO=edvinderp@gmail.com
export EDITOR='nvim'
export BROWSER='brave'
export TERMINAL='kitty'
export MANPAGER='nvim +Man!'

# PATH setup
export PATH="${PATH}:$HOME/.bar:$HOME/.toys:$HOME/.utility:$HOME/.notify:$HOME/.executor"

# Personal use directory paths
export PD="$HOME/Projects"
export KD="$HOME/Projects/META/knowledge"
export CONTAINERVOLUMEDIR="$HOME/mount/Docker_VOLUMES"
export PANDOC_SCHOLAR_PATH="$HOME/Projects/Papers/TEMPLATES/pandoc-scholar"
export BIBLIOGRAPHY_FILE="$HOME/Documents/bibliography.bib"
export SEM="$PD/NITC/Sem8"
export TODODIR="/home/edvin/todos"

# Setup go dev env
export GOROOT="$HOME/.go/go-1.15"
export GOPATH="$HOME/.go/packages"
export PATH="${PATH}:$GOPATH/bin"
export PATH="${PATH}:$GOROOT/bin"
#export GOPATH="$GOPATH:$PD/Go"

export PATH="${PATH}:$HOME/Programs/mongo/bin" 
export PATH="${PATH}:$HOME/.gem/ruby/2.6.0/bin"

# Cargo path
export PATH="$HOME/.cargo/bin:$PATH"
export CROSS_CONTAINER_ENGINE=podman

# LF and pistol
export LF_ICONS="tw=:st=:ow=:dt=:di=:fi=:ln=:or=:ex=:*.c=:*.cc=:*.clj=:*.coffee=:*.cpp=:*.css=:*.d=:*.dart=:*.erl=:*.exs=:*.fs=:*.go=:*.h=:*.hh=:*.hpp=:*.hs=:*.html=:*.java=:*.jl=:*.js=:*.json=:*.lua=:*.md=:*.php=:*.pl=:*.pro=:*.py=:*.rb=:*.rs=:*.scala=:*.ts=:*.vim=:*.cmd=:*.ps1=:*.sh=:*.bash=:*.zsh=:*.fish=:*.tar=:*.tgz=:*.arc=:*.arj=:*.taz=:*.lha=:*.lz4=:*.lzh=:*.lzma=:*.tlz=:*.txz=:*.tzo=:*.t7z=:*.zip=:*.z=:*.dz=:*.gz=:*.lrz=:*.lz=:*.lzo=:*.xz=:*.zst=:*.tzst=:*.bz2=:*.bz=:*.tbz=:*.tbz2=:*.tz=:*.deb=:*.rpm=:*.jar=:*.war=:*.ear=:*.sar=:*.rar=:*.alz=:*.ace=:*.zoo=:*.cpio=:*.7z=:*.rz=:*.cab=:*.wim=:*.swm=:*.dwm=:*.esd=:*.jpg=:*.jpeg=:*.mjpg=:*.mjpeg=:*.gif=:*.bmp=:*.pbm=:*.pgm=:*.ppm=:*.tga=:*.xbm=:*.xpm=:*.tif=:*.tiff=:*.png=:*.svg=:*.svgz=:*.mng=:*.pcx=:*.mov=:*.mpg=:*.mpeg=:*.m2v=:*.mkv=:*.webm=:*.ogm=:*.mp4=:*.m4v=:*.mp4v=:*.vob=:*.qt=:*.nuv=:*.wmv=:*.asf=:*.rm=:*.rmvb=:*.flc=:*.avi=:*.fli=:*.flv=:*.gl=:*.dl=:*.xcf=:*.xwd=:*.yuv=:*.cgm=:*.emf=:*.ogv=:*.ogx=:*.aac=:*.au=:*.flac=:*.m4a=:*.mid=:*.midi=:*.mka=:*.mp3=:*.mpc=:*.ogg=:*.ra=:*.wav=:*.oga=:*.opus=:*.spx=:*.xspf=:*.pdf=:*.nix=:"
export PISTOL_CHROMA_FORMATTER=terminal256
export PISTOL_CHROMA_STYLE=monokai

# export DOCKER_HOST=tcp://dockerman.e.airno.de:2735
# export DOCKER_HOST=ssh://dockerman
# export DOCKER_HOST=unix:///run/user/1000/podman/podman.sock

# export NVM_DIR="$HOME/.nvm"
# [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
