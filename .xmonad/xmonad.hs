import XMonad
import Data.Monoid
import System.Exit

import qualified XMonad.StackSet as W
import qualified Data.Map        as M

import System.IO -- for hClose, hPutStr
import Control.Monad (liftM2) -- focus

-- Layouts/Hooks
import XMonad.Hooks.EwmhDesktops -- (ewmh, ewmhFullscreen, ewmhDesktopsLogHookCustom)
import XMonad.Config.Desktop
import XMonad.Layout.NoBorders
import XMonad.Layout.CenteredMaster
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.ResizableTile
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers

-- Utils
import XMonad.Actions.CycleWS
import XMonad.Util.SpawnOnce
import XMonad.Util.NamedScratchpad
import XMonad.Util.NamedActions
import XMonad.Util.EZConfig
import XMonad.Util.Run  -- for spawnPipe and hPutStrLn

-------------------------------------------------------------------------
--
myTerminal      = "kitty"

myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

myClickJustFocuses :: Bool
myClickJustFocuses = False

myBorderWidth   = 1
myModMask       = mod4Mask

myWorkspaces    = ["1","2","3","4","5","6","7","8","9"]

myNormalBorderColor  = "#1b1f28"
myFocusedBorderColor = "#bf616a"

-------------------------------------------------------------------------
-- ScratchPad

scratchpads = [ (NS "telegram" "telegram-desktop" (className =? "TelegramDesktop") centerFloatboiMed)
              , (NS "terminal" spawnTerm findTerm centerFloatboiSmall)
              , (NS "filemanager" spawnFm findFm centerFloatboiMed)
              ]
              where
                spawnTerm = myTerminal ++ " --name scratchpad"; findTerm = resource =? "scratchpad"
                spawnFm = myTerminal ++ " --name filemanager lf"; findFm = resource =? "filemanager"
                centerFloatboiSmall = customFloating $ W.RationalRect (1/4) (1/4) (1/2) (1/2)
                centerFloatboiMed = customFloating $ W.RationalRect (1/6) (1/6) (2/3) (2/3)




------------------------------------------------------------------------
-- Key bindings
--

showKeybindings :: [((KeyMask, KeySym), NamedAction)] -> NamedAction
showKeybindings x = addName "Show Keybindings" $ io $ do
    h <- spawnPipe "zenity --text-info"
    -- h <- spawnPipe "rofi -dmenu -i -lines 12 -padding 18  -width 60  -location 0 -font 'Noto Sans 8'"
    hPutStr h (unlines $ showKm x)
    hClose h
    return ()

myKeys conf = let
    subKeys str ks = subtitle str : mkNamedKeymap conf ks
    toggleFloat w = windows (\s -> if M.member w (W.floating s)
                    then W.sink w s
                    else (W.float w (W.RationalRect (1/6) (1/6) (2/3) (2/3)) s))
    in

    subKeys "Custom_Misc"
    [ ("M-<Return>"             , addName "Launch Terminal" $                    spawn myTerminal )
    , ("M-S-<Return>"           , addName "Swap window with master"$             windows W.swapMaster)
    , ("M-<Escape>"             , addName "Kill currenty focused program"$       kill)
    -- , ("<Print>"                , addName "Interactive Screenshot" $             (\w -> spawn "/home/wamaral/bin/xmonadpropclick" >> focus w))
    , ("<Print>"                , addName "Interactive Screenshot" $             (spawn "flameshot gui"))
    , ("M-<Print>"              , addName "Full Screenshot" $                    (spawn "~/.utility/screeny-flameshot"))
    , ("M-w"                    , addName "Spawn tmux workspace" $               (spawn "$TERMINAL --class tmux_ws -e tmux new-session -A -s '_shared'"))
    , ("M-C-<Right>"            , addName "Goto next workspace" $                nextWS)
    , ("M-C-<Left>"             , addName "Goto previous workspace" $            prevWS)
    , ("M-C-S-<Right>"          , addName "Shift window to next workspace" $     shiftToNext >> nextWS)
    , ("M-C-S-<Left>"           , addName "Shift window to previous workspace" $ shiftToPrev >> prevWS)
    , ("M-S-p"                  , addName "Rofu launcher" $                      spawn "rofi-apps")
    , ("M-p"                    , addName "Dmenu launcher" $                     spawn "dmenu_run")
    , ("M-M1-c"                 , addName "Launch Config Editor" $               spawn "dmenu-edit-configs.sh")
    , ("M-M1-p"                 , addName "Launch Passmenu" $                    spawn "passmenu")
    , ("M-S-w"                  , addName "Launch Browser" $                     spawn "$BROWSER")
    ] ^++^

    subKeys "Scratchpads"
    [ ("M-y"                    , addName "Telegram Scratchpad" $                namedScratchpadAction scratchpads "telegram")
    , ("M-t"                    , addName "Terminal scratchpad"$                 namedScratchpadAction scratchpads "terminal")
    , ("M-r"                    , addName "File Manager Scratchpad" $            namedScratchpadAction scratchpads "filemanager")
    ] ^++^

    subKeys "Layout Management"
    [ ("M-f"                    , addName "Toggle Fullscreen" $                  sequence_ [ (withFocused $ windows . W.sink), (sendMessage $ Toggle FULL) ])
    , ("M-S-f"                  , addName "Toggle Floating" $                    withFocused toggleFloat)
    , ("M-s"                    , addName "Push window back to tiling mode" $    withFocused $ windows . W.sink)
    , ("M-C-S-<Space>"          , addName "Toggle polybar hide/nohide" $         spawn "polybar-msg cmd toggle" >> sendMessage ToggleStruts)
    , ("M-a"                    , addName "ResizableLayout expand" $             sendMessage MirrorExpand)
    , ("M-z"                    , addName "ResizableLayout Shrink" $             sendMessage MirrorShrink)
    , ("M-M1-x"                 , addName "Xprop" $                              spawn "xprop | zenity --text-info")
    ] ^++^

    subKeys "Function Keys"
    [ ("<XF86AudioMute>"        , addName "Toggle mute/unmute" $                 spawn "pactl set-sink-mute @DEFAULT_SINK@ toggle")
    , ("<XF86AudioLowerVolume>" , addName "Decrease volume" $                    spawn "pactl set-sink-volume @DEFAULT_SINK@ -2%")
    , ("<XF86AudioRaiseVolume>" , addName "Increase Volume" $                    spawn "pactl set-sink-volume @DEFAULT_SINK@ +2%")
    ] ^++^

    subKeys "WM"
    [ ("M-q"                    , addName "PlaceH" $                             spawn "xmonad --recompile && xmonad --restart && notify-send 'Restarted Xmonad Successfully'")
    ]


------------------------------------------------------------------------
-- Layouts:
--

myLayout =  smartBorders
    $ fullScreenToggle
    -- $ fullBarToggle
    $ mirrorToggle
    -- $ reflectToggle
    $ resizeTile ||| centerMaster tiled ||| Full ||| Mirror tiled
  where
    -- fullBarToggle       = mkToggle (single FULLBAR)
    fullScreenToggle    = mkToggle (single FULL)
    mirrorToggle        = mkToggle (single MIRROR)
    -- reflectToggle       = mkToggle (single REFLECTX)
    resizeTile = ResizableTall 1 (3/100) (1/2) []
    -- default tiling algorithm partitions the screen into two panes
    tiled   = Tall nmaster delta ratio

    -- The default number of windows in the master pane
    nmaster = 1

    -- Default proportion of screen occupied by master pane
    ratio   = 1/2

    -- Percent of screen to increment by when resizing panes
    delta   = 3/100

------------------------------------------------------------------------
-- Window rules:
--
-- myManageHook = composeAll
--             [ className =? "Gimp"           --> doFloat
--             , resource  =? "desktop_window" --> doIgnore
--             , resource  =? "kdesktop"       --> doIgnore
--             ]
--             <+> namedScratchpadManageHook scratchpads

myManageHook :: ManageHook
myManageHook =
        manageSpecific
    <+> manageDocks
    <+> namedScratchpadManageHook scratchpads
    where
        manageSpecific = composeAll
            [ 
              resource =? "desktop_window" --> doIgnore
            , className =? "flameshot" -->  (doF W.focusDown <+> doFullFloat)
            , className =? "Gimp"           --> doFloat
            -- , className =? "flameshot"           --> doFloat
            , className =? "Zenity"           --> doFloat
            , className =? "PacketTracer7"           --> doFloat
            , className =? "albert" --> hasBorder False
            , className =? "foobar"      --> doShift "3"
            -- , isFullscreen                  --> doFullFloat
            , resource  =? "kdesktop"       --> doIgnore 
            ]


------------------------------------------------------------------------
-- Startup hook
--
myStartupHook = do
    spawn "$HOME/.config/polybar/launch.sh"
    spawnOnce "nitrogen --restore"

------------------------------------------------------------------------

main  =  do
    xmonad
    $ addDescrKeys ((myModMask, xK_F1), showKeybindings) myKeys
    $ ewmhFullscreen
    $ ewmh
    $ defaultConfig
        {
      -- simple stuff
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,

      -- hooks, layouts
        logHook            = ewmhDesktopsLogHookCustom $ namedScratchpadFilterOutWorkspace,
        layoutHook         = desktopLayoutModifiers $ myLayout,
        manageHook         = myManageHook,
        handleEventHook    = docksEventHook <+> handleEventHook desktopConfig,
	    startupHook        = myStartupHook <+> startupHook desktopConfig
        }

