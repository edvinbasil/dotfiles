#!/bin/sh

wm=${1:-i3}
config_dir="${XDG_CONFIG_HOME:-$HOME/.config}/sxhkd"

pgrep -x sxhkd >/dev/null && { echo "sxhkd already running"; exit;}

sxhkd -c \
    "$config_dir/common.sxhkdrc" \
    "$config_dir/$wm.sxhkdrc" > /tmp/sxhkd.log 2>&1&

