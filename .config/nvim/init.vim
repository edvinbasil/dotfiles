" ----------------------------- Begin -----------------------------------------
"
"           ███╗   ██╗███████╗ ██████╗ ██╗   ██╗██╗███╗   ███╗
"           ████╗  ██║██╔════╝██╔═══██╗██║   ██║██║████╗ ████║
"           ██╔██╗ ██║█████╗  ██║   ██║██║   ██║██║██╔████╔██║
"           ██║╚██╗██║██╔══╝  ██║   ██║╚██╗ ██╔╝██║██║╚██╔╝██║
"           ██║ ╚████║███████╗╚██████╔╝ ╚████╔╝ ██║██║ ╚═╝ ██║
"           ╚═╝  ╚═══╝╚══════╝ ╚═════╝   ╚═══╝  ╚═╝╚═╝     ╚═╝
"
" ---------------------------- Plugins ----------------------------------------


" Install vim-plug if not installed
let plug_install = 0
let autoload_plug_path = stdpath('data') . '/site/autoload/plug.vim'
if !filereadable(autoload_plug_path)
    silent execute '!curl -fLo ' . autoload_plug_path . '  --create-dirs 
      \ "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"'
    execute 'source ' . fnameescape(autoload_plug_path)
    let plug_install = 1
endif
unlet autoload_plug_path


call plug#begin()

Plug 'unblevable/quick-scope'
Plug 'pangloss/vim-javascript'
Plug 'dense-analysis/ale'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'sheerun/vim-polyglot'
" Plug 'terryma/vim-multiple-cursors' " Deprecated in favour of mg979/vim-visual-multi
Plug 'mg979/vim-visual-multi', {'branch': 'master'}
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'
Plug 'machakann/vim-highlightedyank'
Plug 'airblade/vim-rooter'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'sainnhe/gruvbox-material'
Plug 'joshdick/onedark.vim'
Plug 'andymass/vim-matchup'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-commentary'
Plug 'rust-lang/rust.vim'
Plug 'norcalli/nvim-colorizer.lua'
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app & yarn install'  }
Plug 'vim-pandoc/vim-pandoc-syntax'
Plug 'mhinz/vim-startify'
Plug 'godlygeek/tabular'
Plug 'jiangmiao/auto-pairs'
Plug 'junegunn/goyo.vim'
Plug 'tpope/vim-fugitive'
Plug 'glacambre/firenvim', { 'do': { _ -> firenvim#install(0) } }
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }
Plug 'justinmk/vim-sneak'
" Plug 'vimsence/vimsence'
Plug 'mbbill/undotree'
Plug 'ayu-theme/ayu-vim'
Plug 'Yggdroot/indentLine'
Plug 'mhinz/vim-signify'
Plug 'junegunn/gv.vim'
Plug 'pedrohdz/vim-yaml-folds'
" Plug 'romgrk/barbar.nvim' " using the airline plugin instead
" Plug 'ryanoasis/vim-devicons'
Plug 'kyazdani42/nvim-web-devicons'
" Plug 'kyazdani42/nvim-tree.lua' " issues with cwd. issue still open
Plug 'folke/which-key.nvim'
" Plug 'EdenEast/nightfox.nvim'
Plug 'akinsho/nvim-bufferline.lua'
Plug 'NvChad/nvim-base16.lua'
Plug 'beauwilliams/focus.nvim'

call plug#end()

if plug_install
    PlugInstall --sync
endif
unlet plug_install

" --------------------------- Plugin Options ----------------------------------

" Ale Options
" ===========
let g:ale_linters_explicit = 1
 let b:ale_linters = {
 \}
let g:ale_completion_enabled = 1         " Enable completion where enabled.
let g:ale_virtualtext_cursor = 1
let g:ale_set_highlights = 1             " Highlight code in file.
let g:ale_set_quickfix = 1               " Use the quicklist buffer.
let g:ale_set_balloons = 1               " Highlight information on the fly.
let g:ale_set_loclist = 0                " Don't use loclist.
let g:ale_fix_on_save = 1                " Auto fix js.
let g:ale_open_list = 1                  " Auto open quicklist buffer.
let g:ale_list_window_size = 5           " Shrink the suggestion window
let g:ale_completion_max_numbers = 20    " Max suggestions.
let g:airline#extensions#ale#enabled = 1

highlight ALEError ctermfg=167 guifg=#ea6962

" Close quickfix window if it's the last one open.
aug QFClose
  au!
  au WinEnter * if winnr('$') == 1 && getbufvar(winbufnr(winnr()), "&buftype") == "quickfix"|q|endif
aug END

" Airline and gruvbox
" ===================
" set contrast
" this configuration shculd be placed before `colorscheme gruvbox-material`
" available values: 'hard', 'medium'(default), 'soft'
let g:gruvbox_material_background = 'hard'
let g:gruvbox_material_enable_italic = 1
let g:airline_theme = 'edvinbasil'
" let g:airline_theme = 'molokai'
"let g:airline_theme = 'onedark'
let g:airline_powerline_fonts = 1
let g:gruvbox_material_diagnostic_line_highlight = 1
" let g:airline#extensions#tabline#enabled = 1

" nightfox colors
let g:nightfox_style = "nordfox"
let g:nightfox_italic_comments = 1

" ayu
" ==========
let ayucolor="dark" 

" QuickScope
" ==========
" Highlight only once these keys are pressed
let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']
" Set custom quickscope colors: the default ones are hard to distinguish
augroup qs_colors
  autocmd!
  autocmd ColorScheme * highlight QuickScopeSecondary guifg='#da44fc' gui=underline ctermfg=155 cterm=underline
  autocmd ColorScheme * highlight QuickScopePrimary guifg='#5fffff' gui=underline ctermfg=81 cterm=underline
augroup END

" Autopair Options
" ================
" Disable autopair " for vim files for commenting
au filetype vim let b:AutoPairs = {'(':')', '[':']', '{':'}',"'":"'", '`':'`'}

" FZF options
" =========================
" cuz :W keeps messing with my :w
" so make it :w
command! -bang Windows :write

" Sneak Options
" =============
let g:sneak#label = 1

" " VimSence Options
" " =============
" let g:vimsence_small_text = 'NeoVim'
" let g:vimsence_small_image = 'neovim'
" let g:vimsence_editing_details = 'Editing: {}'
" let g:vimsence_editing_state = 'Working on: {}'
" let g:vimsence_file_explorer_text = 'In coc-explorer'
" let g:vimsence_file_explorer_details = 'Browsing Files'
" let g:vimsence_custom_icons = {'filetype': 'iconname'}


" vim-pandoc-syntax Options
" =========================
let g:pandoc#syntax#conceal#use = 0

" Signify Options
" =========================
let g:signify_disable_by_default = 1

" Barbar options
" ===============
" let bufferline.animation = v:false

" nvim-tree options
" =================
let g:nvim_tree_auto_open = 1
let g:nvim_tree_auto_close = 1
let g:nvim_tree_update_cwd = 0
"let g:nvim_tree_respect_buf_cwd = 0

" nvim-bufferline options
"========================
lua << EOF
require("bufferline").setup{
    options = {
        separator_style = "slant",
        middle_mouse_command = "bdelete! %d",
        offsets = {{filetype = "coc-explorer", text = "File Explorer" }}
    }
}
EOF

" focus.nvim options
"========================
lua require("focus").setup()


" ---------------------------- General Settings -------------------------------

 " Set compatibility to Vim only. Unnecessary for nvim, but kept for backwards
 " compatabilty with vim
set nocompatible
" Load fzf
set rtp+=~/.fzf

" set colormode. Important to use with tmux
if exists('+termguicolors')
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
  set termguicolors
endif

" for dark version
set background=dark
" Gimme dat nice colors
colorscheme gruvbox-material
" colorscheme nightfox
" colorscheme ayu
" colorscheme onedark
" colorscheme koehler
" let g:airline_disable_statusline = 1
" Colorizer
" lua require'colorizer'.setup()

set relativenumber
" smarter searching, global replace by default
set ignorecase
set smartcase
" Default g flag for substitution
set gdefault
" incremental search with highlighting
set incsearch
set showmatch
set hlsearch
set autochdir
" Sane splitting
set splitbelow
set splitright
"Always show current position
set ruler
set showtabline=2


" highlight the line the cursor is on
set cursorline
let g:indentLine_char = '⎸'
let g:indentLine_setColors = 0
" let g:indentLine_setConceal = 0

set noshowmode " dont show vim's default visual/insert mde on the last line
set signcolumn=yes " the signcolum toggling gets annoying with continuous lint

" Persistent undo
set undodir=~/.vim/undodir
set undofile

set ttyfast
" Don't redraw while executing macros (good performance config)
set lazyredraw
set synmaxcol=200

" For regular expressions turn magic on
set magic

" Display 5 lines above/below the cursor when scrolling with a mouse.
set mouse=a
set scrolloff=5
" Fixes common backspace problems
set backspace=indent,eol,start

" Display options
set showcmd

" Highlight matching pairs of brackets. Use the '%' character to jump between
" them.
set matchpairs+=<:>

" Display different types of white spaces.
"set list
"set listchars=tab:›\ ,trail:•,extends:#,nbsp:.

" Show line numbers
set number
highlight LineNr ctermfg=black

" Encoding
set encoding=utf-8

" Store info from no more than 100 files at a time, 9999 lines of text
" 100kb of data. Useful for copying large amounts of data between files.
set viminfo='100,<9999,s100

set laststatus=2

" Turn Off Swap Files
" ===================
set noswapfile                " Disable .swp files
set nobackup                  " Disable ~ backup files
set nowritebackup             " No really
set backupdir=/var/tmp,/tmp   " But if you do, write it here
set directory=/var/tmp,/tmp   " Or here 

" TABS vs Spaces

set tabstop=4
set shiftwidth=4
set smarttab
set expandtab
set softtabstop=4
set autoindent

" Completion
" Better display for messages
set cmdheight=2
" You will have bad experience for diagnostic messages when it's default 4000.
set updatetime=300

" Wrapping options
" ================
" If using softwraps, dont break between words
set linebreak
set showbreak=↪\
" visually indent the lines that break
set breakindent

" Turn off soft wrapping
set nowrap
set textwidth=80
" tc: wrap using textwidth; r:continue comments on enter
" q: enable comment formatting with gq; n:detect lists
" b: auto-wrap in insert mode, and do not wrap old long lines
set formatoptions=tcqrn1b
set guioptions+=a
set colorcolumn=80

"set list

" ----------------------------- Bindings --------------------------------------
tnoremap <Esc> <C-\><C-n>
" This is required to preserve Esc behaviour in FZF windows
autocmd! FileType fzf tnoremap <buffer> <esc> <c-c>


" Training guard for arrow keys
noremap <Up> <Nop>
noremap <Down> <Nop>
noremap <Left> <Nop>
noremap <Right> <Nop>

" Left and right can switch buffers
nnoremap <left> :bp<CR>
nnoremap <right> :bn<CR>

" Just dont
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>

" " make ; same as :
" nnoremap ; :
" " remap ; to retain functionality of ;
" nnoremap <space>; ;

" Search options
" append \v to searches  to fix vim's regex handling
nnoremap / /\v
vnoremap / /\v
nnoremap ? ?\v
cnoremap %s/ %sm/

" clear search with ,<space>
nnoremap <leader><space> :noh<cr>
nnoremap <leader>; :Buffers<cr>
nnoremap <leader>g :GFiles<cr>
nnoremap <leader>l :Lines<cr>
nnoremap <leader>b :BLines<cr>
nnoremap <leader>h :History<cr>
nnoremap <leader>r :Rg<space>
nnoremap <leader>, :set invlist<cr>
nnoremap <leader>. :exe  ":FZF " . expand("%:p:h")<cr>
nnoremap <leader>s :SignifyToggle<cr>

nnoremap // :BLines<cr>
nnoremap ?? :Rg<cr>

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" bufferline pick
nnoremap <silent> gb :BufferLinePick<CR>

" " match bracket pairs using tab
" This makes C-i not work to jump forward...so :/
" nnoremap <C-tab> %
" vnoremap <C-tab> %

nnoremap <leader>ev <C-w><C-v><C-l>:e $MYVIMRC<cr>
nnoremap <leader>rv :source $MYVIMRC<cr>

" Move between splits
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Clipboard integration
nnoremap <leader>p "+p
vnoremap <leader>y "+y

nmap <C-p> :Files<CR>
nmap <space>g :GFiles<CR>
nmap <space>b :Buffers<CR>
nmap <space>r :Rg<space>
nmap <space>m :Maps<CR>
nmap <space>h :Helptags<CR>
nmap <space>w z35<CR>

" Comment toggle using NerdCommenter
nmap  gcc
vmap  gc

" Jump to start and end of line using the home row keys
map H ^
map L $

" Tabs be workspace
nnoremap <leader>: :tab split <cr>
nnoremap <leader>1 1gt
nnoremap <leader>2 2gt
nnoremap <leader>3 3gt
nnoremap <leader>4 4gt

" Search results centered please
nnoremap <silent> n nzz
nnoremap <silent> N Nzz
nnoremap <silent> * *zz
nnoremap <silent> # #zz
nnoremap <silent> g* g*zz

" Highlight and move up/down
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" Tab in and out without having to reselect
vnoremap < <gv
vnoremap > >gv

let g:goyo_linenr=1
let g:goyo_height='75%'
nnoremap <F11> :Goyo <cr>

" Y behaves like other caps letters
" yanks from cursor to end of line
nnoremap Y y$

" Add undo break points when typing . or ,
inoremap , ,<c-g>u
inoremap . .<c-g>u


" Fugitive
" ========

nmap <space>gs :G<cr>:10wincmd_<cr>
nmap <space>gj :diffget //3<cr>
nmap <space>gf :diffget //2<cr>

set diffopt+=vertical

" Coc Options
" ==========
" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.

let g:coc_filetype_map = {
    \ 'yaml.ansible': 'yaml',
    \ }
    " \ 'pandoc': 'markdown',

inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()
" Use <Tab> and <S-Tab> to navigate the completion list:
inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" Use <cr> to confirm completion, `<C-g>u` means break undo chain at current
" position. Coc only does snippet and additional edit on confirm.
if exists('*complete_info')
  inoremap <expr> <cr> complete_info()["selected"] != "-1" ? "\<C-y>" : "\<C-g>u\<CR>"
else
  imap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
endif
autocmd! CompleteDone * if pumvisible() == 0 | pclose | endif

nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (&filetype == 'man')
    execute 'Man '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

nmap <leader><F2> <Plug>(coc-rename)
nmap <leader><c-F2> <Plug>(coc-refactor)

" if: inner function; af: all function
xmap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap if <Plug>(coc-funcobj-i)
omap af <Plug>(coc-funcobj-a)

" Use \<TAB> for selections ranges.
nmap <silent> <leader><TAB> <Plug>(coc-range-select)
xmap <silent> <leader><TAB> <Plug>(coc-range-select)

" Remap for format selected region
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Find symbol of current document.
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>

" Search workspace symbols.
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>

" Implement methods for trait
nnoremap <silent> <space>i  :call CocActionAsync('codeAction', '', 'Implement missing members')<cr>

" Show actions available at this location
nnoremap <silent> <space>a  :CocAction<cr>
" Show lists available at this location
nnoremap <silent> <space>l  :CocList<cr>
" Show all diagnostics.
nnoremap <silent> <space>d  :<C-u>CocList diagnostics<cr>

" CodeLens Action
nnoremap <silent> <space>c :<C-u>call CocActionAsync('codeLensAction')<CR>
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" Coc-Explorer
nmap <space>e :CocCommand explorer<cr>
" close explorer if its the last window
autocmd BufEnter * if (winnr("$") == 1 && &filetype == 'coc-explorer') | q | endif

" Startify
" ========

let g:ascii = [
 \ '',
 \ '',
 \ '  ███▄    █     ██▒   █▓    ██▓    ███▄ ▄███▓ ',
 \ '  ██ ▀█   █    ▓██░   █▒   ▓██▒   ▓██▒▀█▀ ██▒ ',
 \ ' ▓██  ▀█ ██▒    ▓██  █▒░   ▒██▒   ▓██    ▓██░ ',
 \ ' ▓██▒  ▐▌██▒     ▒██ █░░   ░██░   ▒██    ▒██  ',
 \ ' ▒██░   ▓██░      ▒▀█░     ░██░   ▒██▒   ░██▒ ',
 \ ' ░ ▒░   ▒ ▒       ░ ▐░     ░▓     ░ ▒░   ░  ░ ',
 \ ' ░ ░░   ░ ▒░      ░ ░░      ▒ ░   ░  ░      ░ ',
 \ '    ░   ░ ░         ░░      ▒ ░   ░      ░    ',
 \ '          ░          ░      ░            ░    ',
 \ '                    ░                         ',
 \ '',
 \ '      -x-   M E M E N T O   M O R I   -x-   ',
 \ '']
let g:startify_custom_header = startify#center(g:ascii)

highlight StartifyHeader guifg=#484C56

" -------------------------- Custom Commands ----------------------------------

" function to show diff of file changes
function! s:DiffWithSaved()
  let filetype=&ft
  diffthis
  vnew | r # | normal! 1Gdd
  diffthis
  exe "setlocal bt=nofile bh=wipe nobl noswf ro ft=" . filetype
endfunction
com! DiffSaved call s:DiffWithSaved()
com! TimeStamp put =strftime('%c')
com! DateStamp put =strftime('%Y-%m-%d')

" function to edit notes
function! s:doto()
  exe ":FZF ~/todos"
endfunction
com! Doto call s:doto()
" --------------- Language specific options + autocmds ------------------------
" Reload sxhkd on config save
autocmd BufWritePost *sxhkdrc silent !pkill -USR1 -x sxhkd && notify-send -i reload "sxhkd reloaded"

augroup filetypedetect
    au BufRead,BufNewFile *.sc set filetype=scheme
    au BufRead,BufNewFile *.sls set filetype=yaml
augroup END

augroup pandoc_syntax
    au! BufNewFile,BufFilePre,BufRead *.md set filetype=markdown.pandoc
    au FileType markdown.pandoc let g:indentLine_setConceal= 0
augroup END

" Rust
" ====
let g:rust_fold = 1
let g:rustfmt_autosave = 1
let g:rust_recommended_style = 1
au Filetype rust setlocal tw=80 cc=80

" GoLang
" ======
if exists('g:loaded_polyglot')
    let g:polyglot_disabled = ['go']
endif


" Script Plugins
au Filetype html,xml,xsl,php source ~/.config/nvim/scripts/closetag.vim

" nvim-base16 lua
" ===============
" lua << EOF
" local base16 = require 'base16'
" require('colors').init('onedark')
" EOF
