#!/bin/bash

# outputs according to mpc playing or paused
if [ "$1" = "status" ]; then
    brown_mpc | grep playing > /dev/null
else
    brown_mpc toggle | grep playing > /dev/null
fi

[ "$?" -eq 0 ] && echo "" || echo ""
