#!/bin/bash

data="$(vnstat  -i eno1+wlo1 -m --oneline)"
total_month="$(cut -d';' -f11 <(echo \"$data\"))"
total_day="$(cut -d';' -f6 <(echo \"$data\"))"

total_month_s="$(echo $total_month | sed 's/\ \([MKGT]\)iB/\1/')"
total_day_s="$(echo $total_day | sed 's/\ \([MKGT]\)iB/\1/')"

echo "%{F#549CDB}$total_day_s%{F#EF6873} : %{F#80D886}$total_month_s%{F-}"
#echo "$total_today/$total_month"
