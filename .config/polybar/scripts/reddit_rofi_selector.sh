#!/bin/bash

output_dir=$(dirname "$0")/outputs
cache_file="$output_dir/reddit_feed_result"

data=$(cat "$cache_file")

srow=0
while true; do
    srow=$(
        echo "$data" | 
        jq -r '.data.children[].data | .subreddit + ":   " + .title' |
        rofi -dmenu -p "search" -format 'i' -selected-row "$srow" -font "Noto Sans 8" -padding 20 -sidebar-mode
    )
    [ "$?" -ne 0 ] && exit

    url="https://reddit.com$(
        echo "$data" | 
        jq -r ".data.children[$srow].data.permalink")"

    #echo "$url"
    xdg-open "$url"
done

