#!/bin/bash

_check(){
    ip link show dblan >/dev/null 2>&1
}

_status(){
    _check && echo "%{B#243348}  %{B-}" || echo "%{B#b22f55}  %{B-}"
}

# outputs according to mpc playing or paused
if [ "$1" = "up" ]; then
    # _check || echo "exec up"
    _check || wg-quick up dblan > /dev/null 2>&1
elif [ "$1" = "down" ]; then
    # _check && echo "exec down"
    _check && wg-quick down dblan > /dev/null 2>&1
else
    true
fi
_status
