#!/bin/bash

# Check caching directory
output_dir=$(dirname "$0")/outputs
cache_file="$output_dir/reddit_feed_result"
[ ! -d "$output_dir" ] && mkdir "$output_dir"

# url of feed
URL="$REDDIT_FEED_URL"
USERAGENT="polybar-scripts/notification-reddit:v1.0 $USER/$HOST"
filter=$(echo "$REDDIT_FEED_TOPIC" | sed -e 's/^/contains("/' -e 's/$/")/' -e 's/:/") or contains("/g')
# File caching for selector
fetch_err=0
curlres=$(curl -sf --user-agent "$USERAGENT" "$URL")
[ "$?" -eq 0 ] && echo "$curlres" > "$cache_file" || fetch_err=1

# Set output color based on fetch
[ "$fetch_err" -eq 0 ] && prim_color="FF4500" || prim_color="00D4FF"
notifications=$(jq "[.data.children[].data | select(.subreddit | $filter).title] | length" "$cache_file")

if [ -n "$notifications" ] && [ "$notifications" -gt 0 ]; then
    echo "%{F#$prim_color} $notifications%{F-}"
else
    echo "%{F#FFFFFF}%{F-}"

fi
