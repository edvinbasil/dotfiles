#!/usr/bin/env bash

function wireless() {
    local interface="wlo1"
    local symbol=""
    local ssid=$(nmcli -t -f active,ssid dev wifi | egrep '^yes' | cut -d\: -f2 | sed -E 's/(.{8})(.{1,})$/\1… /')
    if [[ -z "${ssid}" ]]; then
        echo ""
    else
        echo " ${ssid}"
    fi
}

wireless
