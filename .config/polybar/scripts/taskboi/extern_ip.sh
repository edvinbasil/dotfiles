#!/bin/bash

ip=$(curl -s ipinfo.io | jq -r '.ip')

[ -n "$ip" ] && echo "$ip" || echo "x.x.x.x"
