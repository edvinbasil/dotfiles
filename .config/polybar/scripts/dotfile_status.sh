#!/bin/bash
mod_count="$(/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME status --porcelain | grep -e "^\ \?M" | wc -l)"

[ $mod_count -eq 0 ] && echo "%{F#7DDF6E} %{F-}" || echo "%{F#ED2544} $mod_count%{F-}"
