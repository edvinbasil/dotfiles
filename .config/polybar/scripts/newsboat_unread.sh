#!/bin/bash

output_dir=$(dirname "$0")/outputs
cache_file="$output_dir/newsboat_unread_cache"

get_err=0
newsboat_unread=$(newsboat -x print-unread)
[ "$?" -eq 0 ] && echo "$newsboat_unread" > "$cache_file" || get_err=1


[ "$get_err" -eq 0 ] && prim_color="FF6A00" || prim_color="00D4FF"
feed_count=$(cut -d ' ' -f1 "$cache_file")

[ "$feed_count" -eq 0 ] && echo "%{F#808080} %{F-}" || echo "%{F#$prim_color} $feed_count%{F-}"
