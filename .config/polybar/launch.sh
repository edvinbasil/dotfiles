#!/usr/bin/env bash

name=bar1
pkill polybar
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done
if type "xrandr"; then
  for monitor in $(xrandr --query | grep " connected" | cut -d" " -f1); do
    logfile="/tmp/polybar_$monitor.log"
    echo "--- $monitor" | tee -a "$logfile"
    MONITOR=${monitor} polybar --reload ${name} >>"$logfile" 2>&1 &
  done
else
  polybar --reload ${name} &
fi
echo "Bars launched..."
