# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	. "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

#added by edvin for nodejs
#export PATH=~/.npm-global/bin:$PATH

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
#export PATH="$PATH:$HOME/.rvm/bin"

# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
#export PATH="$PATH:$HOME/.rvm/bin"

[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*


## FOR i3

# My default editor is nano. I don't even able to find exit button in vim :D
 export EDITOR='vim'
 
 # Make ~/.bar folder executable
 export PATH="${PATH}:$HOME/.bar"
 
 # Make ~/.toys folder executable
 export PATH="${PATH}:$HOME/.toys"
 
 # Make ~/.utility folder executable
 export PATH="${PATH}:$HOME/.utility"
 
 # Make ~/.notify folder executable
 export PATH="${PATH}:$HOME/.notify"
 
 # Make ~/.notify folder executable
 export PATH="${PATH}:$HOME/.executor"
 
 # Make Go apps executable
 export GOPATH="$HOME/.go"
 export GOBIN="$HOME/.go/bin"
 export PATH="${PATH}:/usr/local/go/bin"
 export PATH="${PATH}:$HOME/Programs/mongo/bin" 
 RANGER_LOAD_DEFAULT_RC=FALSE
 
 # Disable GTK CSD
 # More explanation here
 # https://github.com/PCMan/gtk3-nocsd
 if [ -n "$STARTUP" ]; then
     BASESTARTUP=${STARTUP%% *}
     BASESTARTUP=${BASESTARTUP##*/}
     if [ "$BASESTARTUP" = x-session-manager ]; then
       BASESTARTUP=$(basename $(readlink /etc/alternatives/x-session-manager))
     fi
     if [ x"$BASESTARTUP" = x"${BASESTARTUP#gnome-session}" ] ; then
       export GTK_CSD=0
       STARTUP="env LD_PRELOAD=/path/to/libgtk3-nocsd.so.0 $STARTUP"
     fi
   fi




#export NVM_DIR="$HOME/.nvm"
#[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm

export PATH="$HOME/.cargo/bin:$PATH"
