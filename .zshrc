# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

typeset -U PATH path
fpath=(~/.zsh/completion $fpath)

# Previously used plugins
# plugins=( sudo cargo docker git-open zsh-syntax-highlighting zsh-z)

FZF_DIR="$HOME/.fzf/bin/"
if [ -d $FZF_DIR ] || hash fzf; then
    # export FZF_DEFAULT_OPTS="--reverse --height 40%"
    export FZF_DEFAULT_OPTS="--height 40% --reverse --ansi"
    if hash rg 2>/dev/null; then
        export FZF_DEFAULT_COMMAND='rg --files --no-ignore --hidden -g "!{.git,node_modules}/*" 2> /dev/null'
    fi
    export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
    # If current selection is a text file shows its content,
    # if it's a directory shows its content, the rest is ignored

    export FZF_CTRL_T_OPTS="--preview-window 'right:60%' --preview 'bat --color=always --style=header,grid --line-range :300 {}' --select-1 --exit-0"
    if hash fd 2>/dev/null; then
        export FZF_ALT_C_COMMAND="fd . -td  2> /dev/null"
    fi

    # export FZF_ALT_C_OPTS="--preview 'tree -C {} | head -200'"
    export FZF_ALT_C_OPTS="--preview 'exa --tree --level=2 --color=always {} | head -200'"
fi

. "$HOME/.acme.sh/acme.sh.env"

# heroku autocomplete setup
HEROKU_AC_ZSH_SETUP_PATH=$HOME/.cache/heroku/autocomplete/zsh_setup && test -f $HEROKU_AC_ZSH_SETUP_PATH && source $HEROKU_AC_ZSH_SETUP_PATH;

###-tns-completion-start-###
if [ -f $HOME/.tnsrc ]; then 
    source $HOME/.tnsrc 
fi
###-tns-completion-end-###

export PATH="$PATH:$HOME/.yarn/bin:$HOME/.config/yarn/global/node_modules/.bin"

export PATH="$PATH:$HOME/.local/bin"
# Override system executables
export PATH="$HOME/bin:$PATH"
# Flyctl
export PATH="$PATH:$HOME/Programs/Fly"

autoload bashcompinit && bashcompinit
source "$HOME/lib/azure-cli/az.completion"
#deno paths
export PATH="$PATH:$HOME/.deno/bin"
export PATH="$PATH:$HOME/Programs/flutter/bin"

export PATH="$PATH:$HOME/.autojump/bin:$HOME/Programs/google-cloud-sdk/bin"
export PATH="${KREW_ROOT:-$HOME/.krew}/bin:$PATH"

export CLOUDSDK_HOME="/home/edvin/Programs/google-cloud-sdk/"
export PATH=$PATH:$HOME/.garden/bin


# autocompletion
# . <(kubectl completion zsh)
# eval "$(starship init zsh)"

# aws cli v2 autocompletion
complete -C '~/.local/bin/aws_completer' aws

# ------------------------- Manual set options -----------------------
export HISTFILE=~/.zsh_history
export HISTSIZE=10000000
export SAVEHIST=10000000

export LESS=-R

setopt HIST_REDUCE_BLANKS
setopt INC_APPEND_HISTORY    # this is default, but set for share_history

setopt AUTO_CD            # Change to a directory just by typing its name
setopt AUTO_PUSHD         # Make cd push each old directory onto the stack
setopt CDABLE_VARS        # Like AUTO_CD, but for named directories
setopt PUSHD_IGNORE_DUPS  # Don't push duplicates onto the stack

setopt INTERACTIVE_COMMENTS # Allow comments in interactive mode

setopt path_dirs
setopt auto_list                # Automatically List Choices On Ambiguous Completion.
setopt auto_param_slash
unsetopt menu_complete   # do not autoselect the first completion entry
unsetopt flowcontrol
setopt auto_menu         # show completion menu on successive tab press
setopt complete_in_word
setopt always_to_end

export LS_COLORS="rs=0:di=01;34:ln=01;36:mh=00:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:mi=00:su=37;41:sg=30;43:ca=30;41:tw=30;42:ow=34;42:st=37;44:ex=01;32:*.tar=01;31:*.tgz=01;31:*.arc=01;31:*.arj=01;31:*.taz=01;31:*.lha=01;31:*.lz4=01;31:*.lzh=01;31:*.lzma=01;31:*.tlz=01;31:*.txz=01;31:*.tzo=01;31:*.t7z=01;31:*.zip=01;31:*.z=01;31:*.dz=01;31:*.gz=01;31:*.lrz=01;31:*.lz=01;31:*.lzo=01;31:*.xz=01;31:*.zst=01;31:*.tzst=01;31:*.bz2=01;31:*.bz=01;31:*.tbz=01;31:*.tbz2=01;31:*.tz=01;31:*.deb=01;31:*.rpm=01;31:*.jar=01;31:*.war=01;31:*.ear=01;31:*.sar=01;31:*.rar=01;31:*.alz=01;31:*.ace=01;31:*.zoo=01;31:*.cpio=01;31:*.7z=01;31:*.rz=01;31:*.cab=01;31:*.wim=01;31:*.swm=01;31:*.dwm=01;31:*.esd=01;31:*.jpg=01;35:*.jpeg=01;35:*.mjpg=01;35:*.mjpeg=01;35:*.gif=01;35:*.bmp=01;35:*.pbm=01;35:*.pgm=01;35:*.ppm=01;35:*.tga=01;35:*.xbm=01;35:*.xpm=01;35:*.tif=01;35:*.tiff=01;35:*.png=01;35:*.svg=01;35:*.svgz=01;35:*.mng=01;35:*.pcx=01;35:*.mov=01;35:*.mpg=01;35:*.mpeg=01;35:*.m2v=01;35:*.mkv=01;35:*.webm=01;35:*.webp=01;35:*.ogm=01;35:*.mp4=01;35:*.m4v=01;35:*.mp4v=01;35:*.vob=01;35:*.qt=01;35:*.nuv=01;35:*.wmv=01;35:*.asf=01;35:*.rm=01;35:*.rmvb=01;35:*.flc=01;35:*.avi=01;35:*.fli=01;35:*.flv=01;35:*.gl=01;35:*.dl=01;35:*.xcf=01;35:*.xwd=01;35:*.yuv=01;35:*.cgm=01;35:*.emf=01;35:*.ogv=01;35:*.ogx=01;35:*.aac=00;36:*.au=00;36:*.flac=00;36:*.m4a=00;36:*.mid=00;36:*.midi=00;36:*.mka=00;36:*.mp3=00;36:*.mpc=00;36:*.ogg=00;36:*.ra=00;36:*.wav=00;36:*.oga=00;36:*.opus=00;36:*.spx=00;36:*.xspf=00;36:"

builtin zstyle ':completion:*:corrections'  format ' %F{green}-- %d (errors: %e) --%f'
builtin zstyle ':completion:*:descriptions' format ' %F{yellow}-- %d --%f'
builtin zstyle ':completion:*:messages'     format ' %F{purple} -- %d --%f'
builtin zstyle ':completion:*:warnings'     format ' %F{red}-- no matches found --%f'
builtin zstyle ':completion:*'              format ' %F{yellow}-- %d --%f'

builtin zstyle ':completion:*' menu select
builtin zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
builtin zstyle ':completion::complete:*' use-cache on
builtin zstyle ':completion:*' cache-path $ZSH_CACHE_DIR


# -------------------------- Plugin Manager -----------------------

typeset -A ZINIT
ZINIT_HOME=$XDG_CACHE_HOME/zsh/zinit
ZINIT[HOME_DIR]=$ZINIT_HOME
ZINIT[ZCOMPDUMP_PATH]=$XDG_CACHE_HOME/zsh/zcompdump

### Added by Zinit's installer
if [[ ! -f $ZINIT_HOME/bin/zinit.zsh ]]; then
        print -P "%F{33}▓▒░ %F{220}Installing %F{33}DHARMA%F{220} Initiative Plugin Manager (%F{33}zdharma/zinit%F{220})…%f"
        git clone https://github.com/zdharma/zinit "$ZINIT_HOME/bin" && \
            zcompile $ZINIT_HOME/bin/zinit.zsh && \
            print -P "%F{33}▓▒░ %F{34}Installation successful.%f%b" || \
            print -P "%F{160}▓▒░ The clone has failed.%f%b"
fi

source "$ZINIT_HOME/bin/zinit.zsh"
autoload -Uz _zinit
(( ${+_comps} )) && _comps[zinit]=_zinit

### End of Zinit's installer chunk
#
# Less verbose commands ---------------------
load=light

# zt: First argument is a wait time and suffix, ie "0a". Anything that doesn't match will be passed as if it were an ice mod. Default ices depth'3' and lucid
zt()  { zinit depth'3' lucid ${1/#[0-9][a-c]/wait"$1"} "${@:2}"; }

# ---------------------------------------------

# Load a few important annexes, without Turbo
# (this is currently required for annexes)
zt light-mode compile'*handler' for \
    zinit-zsh/z-a-as-monitor \
    zinit-zsh/z-a-patch-dl \
    zinit-zsh/z-a-bin-gem-node

zt for \
    OMZ::lib/history.zsh \
    OMZ::lib/key-bindings.zsh

zt 0a light-mode for \
        OMZ::plugins/sudo/sudo.plugin.zsh \
    as'completion' \
        OMZ::plugins/cargo/_cargo \
    as'completion' \
        OMZ::plugins/docker/_docker \
    as'completion' \
        OMZ::plugins/gcloud/gcloud.plugin.zsh \
    as'completion' \
        https://github.com/ahmetb/kubectx/blob/master/completion/kubectx.zsh \
    as'completion' \
        https://github.com/ahmetb/kubectx/blob/master/completion/kubens.zsh \
    as'completion' mv'*.zsh -> _git' \
        felipec/git-completion \
    blockf atpull'zinit creinstall -q .' \
        zsh-users/zsh-completions \
    compile'{src/*.zsh,src/strategies/*}' pick'zsh-autosuggestions.zsh' \
    atload'_zsh_autosuggest_start' \
        zsh-users/zsh-autosuggestions

zt 0b light-mode for \
    compile'{hsmw-*,test/*}' \
        zdharma/history-search-multi-word \
        OMZ::plugins/fasd/fasd.plugin.zsh \
    pick'autopair.zsh' nocompletions atload'bindkey "^H" backward-kill-word; ZSH_AUTOSUGGEST_CLEAR_WIDGETS+=(autopair-insert)' \
        hlissner/zsh-autopair \
    atinit'zicompinit; zicdreplay' atload'FAST_HIGHLIGHT[chroma-man]=' \
        zdharma/fast-syntax-highlighting \
    atload'bindkey "^[[A" history-substring-search-up;
    bindkey "^[[B" history-substring-search-down' \
        zsh-users/zsh-history-substring-search \

zt 0c light-mode for \
    pack'bgn-binary' \
        junegunn/fzf \
    multisrc"shell/{completion,key-bindings}.zsh" \
        id-as"junegunn/fzf_completions" pick"/dev/null" \
        junegunn/fzf

zt 0c light-mode as'null' for \
    sbin \
        paulirish/git-open \

### Other plugins
zinit light-mode lucid wait has"kubectl" for \
  id-as"kubectl_completion" \
  as"completion" \
  atclone"kubectl completion zsh > _kubectl" \
  atpull"%atclone" \
  run-atpull \
    zdharma/null

# Provide A Simple Prompt Till The Theme Loads
PS1="READY >"

zinit ice wait'!' lucid
zinit ice depth=1; zinit $load romkatv/powerlevel10k

zinit ice wait lucid
zinit light endaaman/lxd-completion-zsh

[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

source ~/.aliases
source ~/.token_exports


# direnv: output of `direnv hook zsh`
_direnv_hook() {
  trap -- '' SIGINT;
  eval "$("/home/edvin/.local/bin/direnv" export zsh)";
  trap - SIGINT;
}
typeset -ag precmd_functions;
if [[ -z ${precmd_functions[(r)_direnv_hook]} ]]; then
  precmd_functions=( _direnv_hook ${precmd_functions[@]} )
fi
typeset -ag chpwd_functions;
if [[ -z ${chpwd_functions[(r)_direnv_hook]} ]]; then
  chpwd_functions=( _direnv_hook ${chpwd_functions[@]} )
fi

# fnm
export PATH=/home/edvin/.fnm:$PATH
eval "`fnm env`"

autoload -U +X bashcompinit && bashcompinit
complete -o nospace -C /home/edvin/.local/bin/vault vault
