#!/bin/bash

# Script to run on local machine
# ssh into server, creates backup and copies over the files to $PWD
# removes all except the 2 most recent backups due to storage limits

## This script is not cron-frendly since it gives nome unwanted outputs

backupdir="$HOME/Documents/BACKUPS/iste_techquilla"

[ ! -d "$backupdir" ] && { 
    echo "backup dir not found
    creating directory: $backupdir"
    mkdir -p "$backupdir"
}

if [ "$PWD" != "$HOME/Documents/BACKUPS/iste_techquilla" ];then
    echo "Script not being executed in the correct dir"
    echo "changing into $backupdir"
    cd "$backupdir"
fi

rclone_backup(){
    rclone --include '*.tar.gz' copy . c-drive-iste-nitc: -v
}

[ "$1" = "upload" ] && rclone_backup && exit 0

log=$(ssh iste "\$HOME/wp_backup_edvin.sh")
# dont use head in first command. could cancel the whole command
loghead=$(echo "$log" | head)

echo "
LOGHEAD
=======
$loghead
"

if [ $? -eq 0 ]; then
    name_out=$(echo "$log" | head -n1 | cut -d':' -f2)
    filename="$name_out.gz"
    rsync -aP "iste:$filename" .
    if [ $? -eq 0 ];then
        # ls -t | tail -n +3 gives all except the most recent 2 files 
        # do not mistake the +3
        ssh iste "cd backups; ls -t | tail -n +3 | xargs rm --"
    else
        echo "Error while copying"
    fi
else
    echo "Backing up failed. log is at iste_err.log"
    echo "$log" > ./iste_err.log
    echo "Got filename $filename" | tee -a ./iste_err.log
fi


printf 'Upload backup to gdrive?\n[Y/n]'
read -r choice

[ "$choice" = "n" ] && exit 0

rclone_backup

